# SolarPoweMonitoringSystem

The ESP32 connects to the WiFi Network and uploads the Solar Sensing parameters like Solar Panel Voltage, Temperature, and Light Intensity on Thingspeak Server.

## Materials

| Materials                   | Quantity |
|-----------------------------|----------|
| ESP32 WiFi Module           | 1        |
| Solar Panel (3-25V)         | 1        |
| Voltage Sensor Module       | 1        |
| LM35 Temperature Sensor     | 1        |
| LDR                         | 1        |
| 16x2 I2C LCD Display        | 1        |
| Resistor 2.2K               | 1        |
| Zero PCB/Vero Board         | 1        |
| Micro-USB Data Cable        | 1        |


## Circuit Diagram

![diagram](https://how2electronics.com/wp-content/uploads/2022/11/Solar-Power-Monitoring-System-Block-Diagram-780x289.jpg)

## Required Library

- Thingspeak Server

## Getting Started

### Prerequisites
- [PlatformIO IDE](https://platformio.org/install/ide) or [Arduino IDE](https://www.arduino.cc/en/software)
- ESP32 development board

### Installation
 
1. Clone the repository:
    ```sh
    git clone https://gitlab.com/esp32_esp8266/electricityenergymeter.git
    cd electricityenergymeter
    ```

2. Open the project in PlatformIO or Arduino IDE.

3. Configure your Wi-Fi credentials in main.cpp:
    ```cpp 
    String apiKey = "***************";
    const char* ssid = "**************";  // WiFi Network's SSID
    const char* pass = "**************"; 
    ```

4. Upload the code to your ESP32 board.

### Usage

After uploading, open the Serial Monitor to get the IP address assigned to your ESP32. Open a web browser and navigate to the provided IP address to access the web server.

### Documentation

Detailed setup and configuration instructions can be found in the [setup guide](docs/setup_guide.md).

## Contributing

Contributions are welcome! Please fork this repository and submit pull requests.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
